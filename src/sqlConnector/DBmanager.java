package sqlconnector;

import java.io.FileInputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.TimeZone;

/**
 * manage the connected DataBase
 * 
 * @author Edwin Rozendom
 *
 */
public class DBmanager {

	/**
	 * unique instance of a DBmanager
	 */
	private static DBmanager uniqueInstance = null;

	/**
	 * Connection to DataBase
	 */
	private static Connection con = null;

	/**
	 * generate a new DBmanager
	 * 
	 * Read properties from "database.properties"
	 */
	private DBmanager() {

	
		try {
			
			//great bug
			TimeZone timeZone = TimeZone.getTimeZone("Europe/Paris"); // e.g. "Europe/Rome"
			TimeZone.setDefault(timeZone);
		
			Class.forName("com.mysql.cj.jdbc.Driver").newInstance();

			Properties props = new Properties();
			FileInputStream in = new FileInputStream("database.properties");
			props.load(in);
			in.close();

			String url = props.getProperty("jdbc.url");
			String username = props.getProperty("jdbc.username");
			String password = props.getProperty("jdbc.password");

			con = DriverManager.getConnection(url + "?serverTimezone=Europe/Paris", username, password);
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e){
			e.printStackTrace();
		}
		if (!dbExists()) {
			System.err.println("de database bestaat niet....");
		}
	}

	/**
	 * a getInstance of DBmanager
	 * 
	 * @return DBmanager instance
	 */
	public static synchronized DBmanager getInstance() {
		if (uniqueInstance == null) {
			uniqueInstance = new DBmanager();
		}
		return uniqueInstance;
	}

	/**
	 * check if a Database exists 
	 * 
	 * @return success value 
	 */
	private Boolean dbExists() {
		return con != null;
	}

	/**
	 * close connection to database
	 */
	public void close() {
		try {
			con.close();
			uniqueInstance = null;
			con = null;
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * get the current connection
	 * @return Connection
	 */
	public Connection getConnection() {
		return con;
	}
}

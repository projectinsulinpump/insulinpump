package sqlconnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * DAO to add records to the Device table 
 * 
 * @author Edwin Rozendom
 *
 */
public class DeviceDAO {
	
	private DeviceDAO(){
		// hide implicit public constructor
	}

	/**
	 * add a device to the Database
	 * 
	 * @param deviceID
	 * @param userID
	 * @param key
	 * @param version
	 * 
	 * @return success value 
	 */
	public static boolean addDevice(int deviceID, String key, Double version) {
		try {
			Connection conn = DBmanager.getInstance().getConnection();

			String query = "INSERT INTO Device (DeviceID,Encr_key,Version) VALUES (?,?,?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, deviceID);
			preparedStmt.setString(2, key);
			preparedStmt.setDouble(3, version);

			// execute the preparedstatement
			preparedStmt.execute();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
}

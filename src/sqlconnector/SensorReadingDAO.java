package sqlconnector;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONArray;
import org.json.JSONObject;

/**
 * DAO to add records to the SensorReading table 
 * 
 * @author Edwin Rozendom
 *
 */
public class SensorReadingDAO {

	private SensorReadingDAO(){
		// hide implicit public constructor
	}
	
	/**
	 * Add sensor Reading to the database
	 * 
	 * @param DeviceID
	 * @param currentMillis
	 * @param insulinLevel
	 * @param bloodLevel
	 * 
	 * @return success value 
	 */
	public static boolean addSensorData(int DeviceID, int timeStamp, int insulinLevel) {
		try {
			Connection conn = DBmanager.getInstance().getConnection();

			String query = "INSERT INTO SensorReading (DeviceID,timestmp,insulinLevel) VALUES (?,?,?)";

			PreparedStatement preparedStmt = conn.prepareStatement(query);
			preparedStmt.setInt(1, DeviceID);
			preparedStmt.setInt(2, timeStamp);
			preparedStmt.setDouble(3, insulinLevel);

			// execute the preparedstatement
			preparedStmt.execute();

			return true;
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}

	}
	
	public static boolean cleardata(){
		Connection conn = DBmanager.getInstance().getConnection();
		
		String query1 = "Delete from SensorReading where (DeviceID = 234234);";
		String query2 =  "DELETE FROM device WHERE (DeviceID = 234234);";
		Statement st;
		try {
			st = conn.createStatement();
			st.execute(query1);
			st.execute(query2);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	public static String getSensorData(int DeviceID, long firstTimestamp){
		Connection conn = DBmanager.getInstance().getConnection();
				
		ResultSet result;
		JSONArray jsonString = new JSONArray();

		try {
			
			PreparedStatement getSensor = conn.prepareStatement("SELECT insulinLevel,timestmp FROM (SELECT * FROM SensorReading where (DeviceID = ?) ORDER BY timestmp DESC LIMIT 20 ) sub ORDER BY timestmp ASC");
			
			getSensor.setInt(1,DeviceID);
			
			result = getSensor.executeQuery();
		
			while(result.next()){
				JSONObject jsonObj = new JSONObject();
				jsonObj.put("data", result.getInt(1));
				Date d = new java.util.Date((long)result.getInt(2)*1000);
				SimpleDateFormat formatter = new SimpleDateFormat("kk:mm:ss");
				jsonObj.put("timeStamp",formatter.format(d));
				
				jsonString.put(jsonObj);
			}
				}catch(SQLException e){
		            System.out.println("Message: " + e.getMessage());
		            System.out.println("SQL State: " + e.getSQLState());
			}
		return jsonString.toString();
	}
	
}

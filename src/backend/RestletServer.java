package backend;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashSet;
import java.util.Set;

import org.json.JSONObject;
import org.restlet.Request;
import org.restlet.Response;
import org.restlet.Restlet;
import org.restlet.data.Header;
import org.restlet.data.MediaType;
import org.restlet.data.Method;
import org.restlet.data.Protocol;
import org.restlet.util.Series;

import sqlconnector.DeviceDAO;
import sqlconnector.SensorReadingDAO;

/**
 * Handles HTTP and HTTPS request.
 * 
 * overrides the standard Restlet instance
 * 
 * @author Edwin Rozendom
 *
 */
public class RestletServer extends Restlet {

	/**
	 * the key used for encryption/decryption. (get from DB with multi-users)
	 */
	private String key;

	/**
	 * Timestamp that is received
	 */
	private int receivedTimeStamp = 0;
	
	private int lastcount = 0;
	
	/**
	 * An instance of a new update
	 */
	private Update updateServer = new Update(1.5, "init_temp.lua");

	@Override
	public void handle(Request request, Response response) {
		
		//THE WEBSERVER
		if (request.getMethod().equals(Method.GET) && (request.getProtocol() == Protocol.HTTP)){
			
			//swith what is requested
			switch(request.getResourceRef().getPath()){
			case "/login":
				if(receivedTimeStamp >= ((System.currentTimeMillis() / 1000L) - 60)){
					response.setEntity("True",MediaType.TEXT_PLAIN);
					System.out.println("true");
					receivedTimeStamp = 0;
				} else if (receivedTimeStamp == 0) {
					response.setEntity("False",MediaType.TEXT_PLAIN);
				}
				break;
			case "/last24hours":
				response.setEntity(SensorReadingDAO.getSensorData(234234,System.currentTimeMillis()), MediaType.TEXT_PLAIN);
				break;
			}
		}
		
		if (request.getMethod().equals(Method.POST) && (request.getProtocol() == Protocol.HTTP)){
			JSONObject data = new JSONObject(Encryption.decrypt(key, request.getEntityAsText()));
			receivedTimeStamp = (int) data.get("timestamp");
		}
		
		// filter on put methods
		if (request.getMethod().equals(Method.PUT)) {

			// divide HTTPS and HTTP
			if (request.getProtocol() == Protocol.HTTPS) {
				// get DeviceID
				int deviceID = Integer.parseInt(request.getResourceRef().getPath().replaceAll("\\/", ""));

				// generate an key
				key = Encryption.getRandomKey(16);

				// get version of ESP
				Double currentVersion = Double.parseDouble(request.getClientInfo().getAgent().replace("INSUPUMP/", ""));

				// check for update
				if (updateServer.getLastVersion() > currentVersion) {
					// get lua file from path.
					String luaFile = readFile("updateFiles/" + updateServer.getFilename(), StandardCharsets.UTF_8);

					// set response to key + lua file
					response.setEntity(key.concat(luaFile), MediaType.TEXT_PLAIN);
					
					currentVersion = updateServer.getLastVersion();

				} else {
					// no update, sent only key back over HTTPS
					response.setEntity(key, MediaType.TEXT_PLAIN);
				}
				DeviceDAO.addDevice(deviceID, key, currentVersion);


			} else if (request.getProtocol() == Protocol.HTTP) {
				// receive the JSON data with the HTTP protocol
				
				JSONObject data = new JSONObject(Encryption.decrypt(key, request.getEntityAsText()));
				
				int timestamp = (int) data.get("timestamp");
				int insulinLevel = (int) data.get("insulinLevel");
				int deviceID = (int) data.get("DeviceID");
				int count = (int) data.get("count");
			
				System.out.println(count + "lastcount: " + lastcount);
				if( count > lastcount){
					SensorReadingDAO.addSensorData(deviceID, timestamp,insulinLevel);
					lastcount = count;
				}
			}
		}
		
        //done to fix webserver error
        Set<Method> methods = new HashSet<>();
        methods.add(Method.ALL);

        response.setAccessControlAllowMethods(methods);
        response.setAccessControlAllowCredentials(true);
        Series<Header> reqHeaders = response.getHeaders();
        String requestOrigin = reqHeaders.getFirstValue("Origin", false, "*");
        response.setAccessControlAllowOrigin(requestOrigin);
	}

	/**
	 * read file from path
	 * 
	 * @param path
	 * @param encoding type of encoding(e.g. UTF-8)
	 * @return String, plain-text
	 */
	static String readFile(String path, Charset encoding) {
		byte[] encoded;
		try {
			encoded = Files.readAllBytes(Paths.get(path));
			return new String(encoded, encoding);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
	}
}

package backend;

/**
 * class for update related content
 * 
 * @author Edwin Rozendom
 *
 */
public class Update {

	/**
	 * last version that is stored on server
	 */
	private double lastVersion;
	
	/**
	 * the filename of the update
	 */
	private String filename;

	/**
	 * constructor for new update
	 * 
	 * @param lastVersion
	 * @param filename
	 */
	public Update(double lastVersion, String filename){
		this.lastVersion = lastVersion;
		this.filename = filename; 
	}
	
	public double getLastVersion() {
		return lastVersion;
	}
	public void setLastVersion(double lastVersion) {
		this.lastVersion = lastVersion;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}	
}

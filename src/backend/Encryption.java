package backend;

import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;

/**
 * Used to perform encryption related tasks
 * 
 * @author Edwin Rozendom
 *
 */
public final class Encryption {

	private Encryption() {
		// hide implicit public constructor
	}

	/**
	 * generate A random key Used for a IV vector
	 * 
	 * @param numCharacters
	 *            the number of random characters generated.
	 * 
	 * @return Random string
	 */
	public static String getRandomKey(int numCharacters) {
		try {
			String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789~`!@#$%^&*()-_=+[{]}\\|;:\'\",<.>/?";
			return RandomStringUtils.random(numCharacters, 0, 0, false, false, characters.toCharArray(),
					SecureRandom.getInstanceStrong());
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
			return null;
		}
	}

	/**
	 * decrypt a message from "AES-CBC"
	 * 
	 * @param key,
	 *            the key for encryption/decryption
	 * @param encrypted,
	 *            the encrypted data, base64
	 * @return decoded plain text
	 */
	public static String decrypt(String key, String encrypted) {
		try {
			String[] parts = encrypted.split("~");
			String ivStr = parts[0];
			String encryptedXML = parts[1];
			IvParameterSpec iv = new IvParameterSpec(ivStr.getBytes());
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
			System.arraycopy(cipher.getIV(), 0, encryptedXML.getBytes(), 0, cipher.getIV().length);

			String decrypted = new String(cipher.doFinal(Base64.decodeBase64(encryptedXML)));

			return decrypted.replaceAll("(?m)^[ \t]*\r?\n", "").trim();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}

	/**
	 * encrypt plain text with "AES-CBC"
	 * 
	 * @param key,
	 *            the key for encryption/decryption
	 * @param IV,
	 *            the vector that is used as IV
	 * @param value,
	 *            plain text for encryption
	 * @return encrypted data, sbase64
	 */
	public static String encrypt(String key, String IV, String value) {
		try {
			SecretKeySpec skeySpec = new SecretKeySpec(key.getBytes("UTF-8"), "AES");

			Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
			cipher.init(Cipher.ENCRYPT_MODE, skeySpec, new IvParameterSpec(IV.getBytes("UTF-8")));

			byte[] encrypted = cipher.doFinal(value.getBytes());

			return Base64.encodeBase64String(encrypted);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

		return null;
	}
}
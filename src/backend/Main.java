package backend;

import org.restlet.Component;
import org.restlet.Server;
import org.restlet.data.Parameter;
import org.restlet.data.Protocol;
import org.restlet.util.Series;

import sqlconnector.SensorReadingDAO;


/**
 * main class
 * 
 * @author Edwin Rozendom
 *
 */
public class Main {

	public static void main(String[] args) throws Exception {
		// Create a new Component.
		Component component = new Component();
		component.getServers().add(Protocol.HTTP, 8181);

		// Add a new HTTPS server listening on port 8183
		Server server = component.getServers().add(Protocol.HTTPS, 8183);

		// Parameters for HTTPS
		Series<Parameter> parameters = server.getContext().getParameters();
		parameters.add("sslContextFactory", "org.restlet.engine.ssl.DefaultSslContextFactory");
		parameters.add("keyStorePath", "C:/Users/Edwin/Documents/webServer/keystore.jks");
		parameters.add("keyStorePassword", "Welkom123");
		parameters.add("keyPassword", "Welkom123");
		parameters.add("keyStoreType", "JKS");

		// Create a new RestletServer
		RestletServer restlet = new RestletServer();

		SensorReadingDAO.cleardata();
		 
		// Start the component
		component.getDefaultHost().attach("", restlet);
		component.start();

	}

}
